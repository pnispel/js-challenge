import * as actions from '../constants/actions';
import Dexie from 'dexie';

Dexie.Promise.on('error', (err) => {
  console.log(`Uncaught error: ${err}`);
});

const db = new Dexie('contacts');

db.version(1).stores({
  contacts: 'id,first_name,last_name,date_of_birth,phone,email,notes',
});

db.open();

// -------------------------------------------------------------------------- //

export function initContacts() {
  return (dispatch) => {
    db.contacts.toArray((contacts) => {
      dispatch({ type: actions.INIT_CONTACTS, contacts });
    });
  };
}

export function addContact(contact) {
  return (dispatch, getState) => {
    const { contacts } = getState();

    contact.id = contacts.items.reduce((maxId, c) => Math.max(c.id, maxId), -1) + 1;

    db.contacts.add(contact);

    dispatch({ type: actions.ADD_CONTACT, contact });
  };
}

export function changeSort(field, direction) {
  return { type: actions.CHANGE_SORT, field, direction };
}

export function updateSearch(query) {
  return { type: actions.UPDATE_SEARCH, query };
}
