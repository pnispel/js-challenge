import React from 'react';
import classnames from 'classnames';

import './style.scss';

export default class Button extends React.Component {
  generateContainerClasses() {
    const className = this.props.className;

    return classnames({
      button: true,
      noselect: true,
      [className]: (className && true),
    });
  }

  generateButtonClasses() {
    const icon = this.props.icon;
    const hasChildren = (icon && !!this.props.children);
    const hasIcon = !!icon;

    return classnames({
      fa: hasIcon,
      'with-children': (hasIcon && hasChildren),
      [icon]: hasIcon,
    });
  }

  render() {
    return (
      <div className={this.generateContainerClasses()} onClick={this.props.onClick}>
        <span className={this.generateButtonClasses()}></span>

        {this.props.children}
      </div>
    );
  }
}

Button.propTypes = {
  className: React.PropTypes.string,
  icon: React.PropTypes.string,
  onClick: React.PropTypes.func,
};
