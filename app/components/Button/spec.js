import React from 'react';
import TestUtils from 'react-addons-test-utils';
import ReactDom from 'react-dom';
import expect from 'expect';

import Button from './index.jsx';

function hasClass(element, cls) {
  return (` ${element.className} `).indexOf(` ${cls} `) > -1;
}

describe('Button Component', function() {
  beforeEach(() => {
    this.component = TestUtils.renderIntoDocument(
      <Button
          className="test"
          icon="test-icon">
        <div className="test-componenet"></div>
        <div className="test-componenet"></div>
        <div className="test-componenet"></div>
      </Button>
    );
    this.renderedDOM = () => ReactDom.findDOMNode(this.component);
  });

  it('renders without problems', () => {
    const root = this.renderedDOM();
    expect(root).toExist();
  });

  it('places children inside of the component', () => {
    const testComponents =
      this.renderedDOM().querySelectorAll('div.test-componenet');

    expect(this.renderedDOM().children.length).toEqual(4);
    expect(testComponents.length).toEqual(3);
  });

  it('places the test class on the new component', () => {
    const isRightComponent =
      hasClass(this.renderedDOM(), 'test');

    expect(isRightComponent).toBe(true);
  });

  it('adds the icon to the new element', () => {
    const testComponents =
      this.renderedDOM().querySelectorAll('span.test-icon');

    expect(testComponents.length).toEqual(1);
  });
});
