import React from 'react';
import classnames from 'classnames';

import Button from '../Button';

import './style.scss';

export default class Modal extends React.Component {
  constructor(...props) {
    super(...props);

    this.state = {
      hasError: false,
      errorText: '',
    };

    this.clearError = this.clearError.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate() {
    const $body = document.body;
    const classes = $body.className;

    if (this.props.visible) {
      $body.className += ' modal-open';
    } else {
      $body.className = classes.replace(/(?:^|\s)modal-open(?!\S)/g, '');
    }
  }

  handleSubmit(e) {
    const validated = this.props.onSubmit(e);

    if (validated === true) return;

    this.setState({
      hasError: true,
      errorText: validated,
    });
  }

  clearError() {
    this.setState({
      hasError: false,
      errorText: '',
    });
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  render() {
    const className = this.props.className;

    return (
      <div
        className={classnames({
          'modal-container': true,
          closed: !this.props.visible,
          [className]: !!className,
        })}
        onClick={this.props.closeModal}
      >

        <div
          className="modal"
          onClick={this.stopPropagation}
        >
          <div className="modal-header">
            <span className="modal-title">{this.props.title}</span>
            <span
              className="modal-close fa fa-remove"
              onClick={this.props.closeModal}
            ></span>
          </div>

          <div className="modal-body">
            {this.state.hasError && (
              <div className="modal-error">
                {this.state.errorText}

                <span
                  className="error-close fa fa-remove"
                  onClick={this.clearError}
                ></span>
              </div>
            )}

            {this.props.children}
          </div>

          <div className="modal-footer">
            <Button className="save-button" onClick={this.handleSubmit}>
              {this.props.actionTitle}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  visible: React.PropTypes.bool,
  title: React.PropTypes.string,
  actionTitle: React.PropTypes.string,
  onSubmit: React.PropTypes.func,
  className: React.PropTypes.string,
  closeModal: React.PropTypes.func,
};
