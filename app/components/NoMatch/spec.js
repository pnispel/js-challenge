import React from 'react';
import TestUtils from 'react-addons-test-utils';
import ReactDom from 'react-dom';
import expect from 'expect';

import NoMatch from './index.jsx';

describe('root', () => {
  beforeEach(function() {
    this.component = TestUtils.renderIntoDocument(<NoMatch />);
    this.renderedDOM = () => ReactDom.findDOMNode(this.component);
  });

  it('renders without problems', function() {
    const root = this.renderedDOM();
    expect(root).toExist();
  });
});
