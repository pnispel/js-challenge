import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as appActions from '../../actions';

import Table from '../Table';

import './style.scss';

class Page extends React.Component {
  render() {
    return (
      <div className="wrapper">
        <header className="page-header">
          <span className="header-title">Contacts Keeper</span>
        </header>

        <Table {...this.props} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    contacts: state.contacts.items,
    searchItems: state.contacts.searchItems,
    sortField: state.contacts.sortField,
    sortDirection: state.contacts.sortDirection,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);
