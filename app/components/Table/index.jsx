import React from 'react';
import classnames from 'classnames';

import Button from '../Button';
import Modal from '../Modal';

import './style.scss';

export default class Table extends React.Component {
  constructor(...props) {
    super(...props);

    this.state = {
      modalVisible: false,
    };

    this.handleSortHeaderClick = this.handleSortHeaderClick.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleAddClick = this.handleAddClick.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleModalSubmit = this.handleModalSubmit.bind(this);
  }

  handleCloseModal() {
    this.setState({
      modalVisible: false,
    });
  }

  handleModalSubmit() {
    let validated = true;
    const refs = this.refs;
    const newContact = {
      first_name: refs.first_name.value,
      last_name: refs.last_name.value,
      date_of_birth: refs.date_of_birth.value,
      phone: refs.phone.value,
      email: refs.email.value,
      notes: refs.notes.value,
    };

    Object.keys(newContact).map(k => {
      if (newContact.hasOwnProperty(k) && !newContact[k]) {
        validated = `Missing Property`;
      }
    });

    if (validated !== true) return validated;

    this.props.actions.addContact(newContact);

    this.setState({
      modalVisible: false,
    });
  }

  handleAddClick() {
    this.setState({
      modalVisible: true,
    });
  }

  handleSearchChange(e) {
    const text = e.target.value;

    this.props.actions.updateSearch(text);
  }

  generateTableHeaderClasses(title) {
    const sortField = this.props.sortField;
    const isThisField = sortField === title;
    const sortDirection = this.props.sortDirection;

    return classnames({
      filtered: isThisField,
      fa: isThisField,
      'fa-caret-up': isThisField && sortDirection === 1,
      'fa-caret-down': isThisField && sortDirection === -1,
    });
  }

  handleSortHeaderClick(title) {
    return () => {
      const sortDirection = this.props.sortDirection;

      this.props.actions.changeSort(title, sortDirection * -1);
    };
  }

  renderContacts() {
    const searchItems = this.props.searchItems;
    const contacts = (searchItems && searchItems.length) ? searchItems : this.props.contacts;

    return contacts.map((contact) => (
        <tr key={contact.id}>
          <td>{contact.first_name}</td>
          <td>{contact.last_name}</td>
          <td>{contact.date_of_birth}</td>
          <td>{contact.phone}</td>
          <td>{contact.email}</td>
          <td>{contact.notes}</td>
        </tr>
      )
    );
  }

  render() {
    return (
      <div className="table-container">
        <Modal
          className="add-contact-modal"
          visible={this.state.modalVisible}
          title="Contacts Keeper"
          actionTitle="Save"
          closeModal={this.handleCloseModal}
          onSubmit={this.handleModalSubmit}
        >
          <div className="add-contact-modal-body">
            <div className="form-field-group">
              <div className="form-field">
                <label className="form-label">First Name</label>
                <input type="text" ref="first_name"/>
              </div>

              <div className="form-field">
                <label className="form-label">Last Name</label>
                <input type="text" ref="last_name"/>
              </div>
            </div>

            <div className="form-field-group">
              <div className="form-field">
                <label className="form-label">Date of Birth</label>
                <input type="date" ref="date_of_birth"/>
              </div>

              <div className="form-field">
                <label className="form-label">Phone Number</label>
                <input type="tel" ref="phone"/>
              </div>
            </div>

            <div className="form-field">
              <label className="form-label">Email</label>
              <input type="email" ref="email"/>
            </div>

            <div className="form-field">
              <label className="form-label">Notes</label>
              <textarea type="text" ref="notes" />
            </div>
          </div>
        </Modal>

        <div className="table-interactions">
          <div className="search-bar">
            <input
              type="text"
              className="search square"
              placeholder="Search"
              onChange={this.handleSearchChange}
            />

            <Button icon="fa-search" className="search-button" />
          </div>
          <div className="add-contact-button">
            <Button
              icon="fa-plus"
              className="search-button"
              onClick={this.handleAddClick}
            >
              Contacts Keeper
            </Button>
          </div>
        </div>
        <div className="table">
          <table>
            <tbody>
              <tr>
                <th onClick={this.handleSortHeaderClick('first_name')}>
                  <span className={this.generateTableHeaderClasses('first_name')}></span>
                  First Name
                </th>
                <th onClick={this.handleSortHeaderClick('last_name')}>
                  <span className={this.generateTableHeaderClasses('last_name')}></span>
                  Last Name
                </th>
                <th onClick={this.handleSortHeaderClick('date_of_birth')}>
                  <span className={this.generateTableHeaderClasses('date_of_birth')}></span>
                  Date of Birth
                </th>
                <th onClick={this.handleSortHeaderClick('phone')}>
                  <span className={this.generateTableHeaderClasses('phone')}></span>
                  Phone Number
                </th>
                <th onClick={this.handleSortHeaderClick('email')}>
                  <span className={this.generateTableHeaderClasses('email')}></span>
                  Email
                </th>
                <th onClick={this.handleSortHeaderClick('notes')}>
                  <span className={this.generateTableHeaderClasses('notes')}></span>
                  Notes
                </th>
              </tr>

              {this.renderContacts()}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

Table.propTypes = {
  actions: React.PropTypes.object,
  sortField: React.PropTypes.string,
  sortDirection: React.PropTypes.number,
  searchItems: React.PropTypes.array,
  contacts: React.PropTypes.array,
};
