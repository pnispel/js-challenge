export const ADD_CONTACT = 'ADD_CONTACT';
export const CHANGE_SORT = 'CHANGE_SORT';
export const UPDATE_SEARCH = 'UPDATE_SEARCH';
export const INIT_CONTACTS = 'INIT_CONTACTS';
