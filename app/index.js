import React from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import 'normalize.css';
import 'font-awesome-webpack';

import Page from './components/Page';
import NoMatch from './components/NoMatch';

import store from './store';

render((
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={Page} />
      <Route path="*" component={NoMatch}/>
    </Router>
  </Provider>
), document.getElementById('app'));
