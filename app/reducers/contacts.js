import {
  ADD_CONTACT,
  CHANGE_SORT,
  UPDATE_SEARCH,
  INIT_CONTACTS,
} from '../constants/actions';

import Fuse from 'fuse.js';

const initialState = {
  sortField: 'first_name',
  sortDirection: -1,
  searchItems: [],
  items: [],
};

export default function contacts(state = initialState, action) {
  const sortField = action.field || state.sortField;
  let sortDirection = action.direction || state.sortDirection;

  switch (action.type) {
    case INIT_CONTACTS:
      return {
        searchItems: state.searchItems,
        items: action.contacts,
        sortField,
        sortDirection,
      };

    case ADD_CONTACT:
      return {
        items: [
          action.contact,
          ...state.items,
        ],
        sortField,
        sortDirection,
      };

    case CHANGE_SORT:
      sortDirection = sortField === state.sortField ? sortDirection : 1;

      const sortedState = state.items.slice(0).sort((aa, bb) => {
        const a = aa[sortField];
        const b = bb[sortField];

        if (a === b) {
          return 0;
        }

        if (typeof a === typeof b) {
          return a < b ? (-1 * sortDirection) : (1 * sortDirection);
        }

        return typeof a < typeof b ? (-1 * sortDirection) : (1 * sortDirection);
      });

      return {
        sortField,
        sortDirection,
        items: sortedState,
      };

    case UPDATE_SEARCH:
      const options = {
        keys: ['first_name', 'last_name'],
      };

      const f = new Fuse(state.items, options);

      return {
        searchItems: f.search(action.query) || [],
        items: state.items,
        sortField,
        sortDirection,
      };

    default:
      return state;
  }
}
